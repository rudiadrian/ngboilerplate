module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ['jasmine'],
        files: [
            'js/vendor/angular/angular.js',
            'js/vendor/angular-mocks/angular-mocks.js',
            'js/vendor/angular-ui-router/release/angular-ui-router.js',
            'js/src/**/*.module.js',
            'js/src/**/*.js'
        ],
        port: 8080,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['PhantomJS'],
        singleRun: true
    });
};