(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'paths'];

    function configure ($stateProvider, $urlRouterProvider, paths) {
        $stateProvider
        .state('app.dashboard', {
            url: '/dashboard',
                templateUrl: paths.TEMPLATES + '/views/dashboard.html',
        });

        $urlRouterProvider.otherwise('/app/dashboard');
    }
}());
