(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = [];

    function DashboardController () {
        var vm = this;
        vm.users = [];
        vm.addUser = addUser;

        function addUser(user) {
            if (typeof user.name !== 'undefined' && user.name.length > 0) {
                vm.users.push(user);
            }
        }
    }
}());
